package com.kps.spring.demo.service;

import com.kps.spring.demo.model.Article;

import java.util.List;

public interface ArticleService {
    List<Article> getAll();
    Article fineOne(Integer id);
    void update(Article article);
}
