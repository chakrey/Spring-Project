package com.kps.spring.demo.service.implement;

import com.kps.spring.demo.model.Article;
import com.kps.spring.demo.repository.ArticleRepo;
import com.kps.spring.demo.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class ArticleServiceImple implements ArticleService {

    private ArticleRepo articleRepo;

    @Autowired
    public ArticleServiceImple(ArticleRepo articleRepo){
        this.articleRepo = articleRepo;
    }
    @Override
    public List<Article> getAll() {
        return this.articleRepo.getAll();
    }

    @Override
    public Article fineOne(Integer id) {
        return this.articleRepo.fineOne(id);
    }

    @Override
    public void update(Article article) {
        articleRepo.update(article);
    }
}
