package com.kps.spring.demo.repository;

import com.github.javafaker.Faker;
import com.kps.spring.demo.model.Article;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ArticleRepo {
    List<Article> articles = new ArrayList<>();
    Faker fake = new Faker();

    {
        for (int i=1; i<11 ; i++){
            Article article = new Article();

            article.setId(i);
            article.setTitle(fake.book().title());
            article.setAuthor(fake.book().author());
            article.setPublisher(fake.book().publisher());

            articles.add(article);
        }
    }
    public List<Article> getAll(){
        return this.articles;
    }
    public Article fineOne(Integer id){
        for (int i=0; i<articles.size(); i++){
            if(articles.get(i).getId()==id){
                return  articles.get(i);
            }

        }
        return null;
    }

    public boolean update(Article article){
        for (int i =0 ; i<articles.size();i++){
            if(articles.get(i).getId()==article.getId()){
                articles.set(i,article);
                return true;
            }
        }
        return false;
    }

}
