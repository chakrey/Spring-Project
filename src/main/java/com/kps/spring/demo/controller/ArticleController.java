package com.kps.spring.demo.controller;

import com.kps.spring.demo.model.Article;
import com.kps.spring.demo.service.ArticleService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class ArticleController {

    private ArticleService articleService;

    @Autowired
    public ArticleController(ArticleService articleService){
        this.articleService=articleService;
    }

    @GetMapping("/index")
    public String index(ModelMap model){
        List<Article> articles = this.articleService.getAll();

        model.addAttribute("articles",articles);
        return "article/index";
    }

    @GetMapping("view/{id}")
    public String view(@PathVariable("id") Integer id, ModelMap model){
        System.out.println("ID: "+id);
        Article article = this.articleService.fineOne(id);
        model.addAttribute("article",article);
        return "article/view-Detail";
    }
    @GetMapping("/update/{article_id}")
    public String update(@PathVariable Integer article_id, ModelMap model){
        System.out.println("ID to update"+article_id);
        Article articles = this.articleService.fineOne(article_id);
        System.out.println(articles);
        model.addAttribute("articles",articles);
        return "article/update";
    }

    @PostMapping("update/submit")
    public String updateSubmit(@ModelAttribute Article article){
        articleService.update(article);
        return "redirect:/index";
    }




}




